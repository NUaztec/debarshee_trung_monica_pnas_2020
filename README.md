This repository contains the data associated with the paper:

Debarshee Bagchi, Trung Dac Nguyen, Monica Olvera de la Cruz, Surface polarization effects in confined polyelectrolyte solutions, Proc. Natl. Acad. Sci. USA 2020

* Correspondence should be addressed to m-olvera@northwestern.edu

