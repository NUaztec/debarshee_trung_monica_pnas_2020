
set term post color solid eps enhanced ",34"
set out "QV.eps"

set xtics 0.05 

set label 11 "(b)" at 0.01,52

set xlabel "{/Symbol S} [Cm^{-2}]" font ",32"
set ylabel "{/Symbol F}_+ [mV]" font ",36" offset 1.5,0.0

set key bottom right samplen 0.76 spaci 1.6
set multiplot
set size 1.0,1.0
set origin 0.0,0.0

p[:0.20][:] "wp_Capa.d" u 1:($4*1000) w lp pt 5 ps 2 lc rgb "red" t"Polyelectrolyte w/Polr","wop_Capa.d" u 1:($4*1000) w lp pt 7 ps 2 lc rgb "blue" t"Polyelectrolyte wo/Polr"

unset key

unset multiplot
q
