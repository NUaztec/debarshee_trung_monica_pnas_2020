#q = 1.6e-19; x3 = 0.027e-9;
q = 13.656549; x3 = 1;

set label 11 "(c)" at 0.1525,3.15

set term post color enhanced solid eps ",26"

#set zlabel "{/Symbol r}(x,y) [reduced unit]" #offset 1.6,0.0
set xlabel "{/Symbol S} [C/m^2]"
set ylabel "Fourth moment {/Symbol k}"


set ytics 0.1
set key cent cent sample 0.85 spacing 1.5

 set output "fourth.eps"; 

p[] "wop_fourth.d" w lp pt 5 ps 3 lc rgb "red" t"wo/Polr","wp_fourth.d" w lp pt 7 ps 3 lc rgb "blue" t"w/Polr"

q
