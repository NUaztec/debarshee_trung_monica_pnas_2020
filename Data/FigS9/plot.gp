set key at 0.1,4.85 sample 0.25 spacing 1.8

set xtics 0.1

#set title "Rigid Polymer"
#set label "(b)" at 0.18,0.5

set ytics 20

set xlabel "Q_+ [Cm^{-2}]" font ",32"
set ylabel "{/Symbol F}_+ [mV]" font ",36" offset 1.5,0.0

p[][:180] "Capa.d" u (-$6):($4*1000) w lp pt 5 ps 3 lc rgb "red" t""

set term post color solid eps enhanced ",32"
set output "ConstPot5000.eps"
repl
q
