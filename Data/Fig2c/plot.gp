

set ylabel "{/Symbol r}_c(z) / {/Symbol S}" offset 1.5,0.0
set xlabel "z / H"

set label 11 "(c)" at 0.46,-1.2

#set ytics 0.3
set xtics 0.2

set key at 0.7,-0.1 sample 0.85 spacing 1.75

p "ov0.30" eve 1 u ($1/100):3 w l lw 3 lc rgb "red" t"f_q = 0.30", "ov0.60" eve 1 u ($1/100):3 w l lw 3 lc rgb "dark-green" t"f_q = 0.60", "ov0.90" eve 1 u ($1/100):3 w l lw 3 lc rgb "blue" t"f_q = 0.90"
repl 0.0 w l lt -1 t"", -1.0 w l lt -1 t""

set term post color enhanced eps ",36"
set output "overcharge.eps"
repl
q
