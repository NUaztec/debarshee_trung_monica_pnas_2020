
q = 13.656549; x3 = 1;



set label 11 "(c)" at 0.02, 1.3

set term post color enhanced eps ",30"
set output "Zvarn_Rgz_mul.eps"

set multiplot

set key top right spacing 1.25

set size 1,1
set origin 0,0

set ytics 0.2
set xtics 0.1

set ylabel "3 Rg_z^2(z)/Rg^2(z)" offset 1.6,0.0
set xlabel "z / H"

p[0:0.5][0.0:] "zvar04_BadSolvent.d" u 1:(3*$5/$2) w l lw 4 lc rgb "red" t"Polyeletrolyte w/Polr",1.0 w l lt -1 t"", "zvar04_NoPolr.d" eve ::::142 u 1:(3*$5/$2) w l lw 5 dt 4 lc rgb "blue" t"Polyeletrolyte wo/Polr", 0 w l lt -1 t""

set size 0.7,0.335
set origin 0.265,0.371

unset label 11

set ytics 0.15
set xtics 0.1

unset key
unset xlabel
unset ylabel

set arrow 1 from 0.055,0.22 to 0.055,-0.08 lw 2 nohead
set arrow 2 from 0.105,0.22 to 0.105,-0.08  lw 2 nohead
set arrow 3 from 0.180,0.22 to 0.180,-0.08  lw 2 nohead

set ytics("1" 1)
unset xtics

set label 11 "3 Rg_z^2(z)/Rg^2(z)" at 0.25, 0.70 font ",26"

p[0:0.5][] "zvar04_BadSolvent.d" u 1:(3*$5/$2) w l lw 4 lc rgb "red" t"",1.0 w l lt -1 t""

set size 0.7,0.35
set origin 0.265,0.155

unset label 11

set ytics("0" 0)
set xtics 0.10 font ",24"

set label 12 "{/Symbol r}(z)"  at 0.3, -0.03 font ",28"
p[0:0.5][] "asd0.40_BadSolvent"eve 1 u 1:($2*q/x3) w l lw 4 lc rgb "dark-green" t"", 0 w l lt -1 t""

unset multiplot
q
