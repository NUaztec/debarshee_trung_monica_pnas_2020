#q = 1.6e-19; x3 = 0.027e-9;
q = 13.656549; x3 = 1;

set term post color enhanced solid eps ",28"
set output "GoodPoor.eps"


set multiplot
set size 1,1
set origin 0,0


set label 11 "(c)" at 0.02,0.20

set ylabel "{/Symbol r}(z) [reduced unit]" offset 1.6,0.0
set xlabel "z / H"

set ytics 0.1

#set arrow 123 from 0.13, 0.05 to 0.13, 0.005 lw 2
#set label 111 "EDL" at 0.09,0.07 font ",20"


set key bottom right sample 0.95 spacing 1.65

p[-0.05:1.05][-0.25:0.25] "poor_asd0.60" u 1:($2*q/x3) w l lw 2 lc rgb "red" t"Poor", "good_asd0.60" u 1:($2*q/x3) w l lw 2 lc rgb "blue" t"Good", 0.0 w l lt -1 t""

unset label 11

set size 0.45,0.4
set origin 0.23,0.17

unset arrow 123

set ytics 0.1
set xtics 0.05
unset key

unset ylabel
unset xlabel

p[-0.:0.1][-0.25:]"poor_asd0.60" u 1:($2*q/x3) w l lw 2 lc rgb "red" t"","good_asd0.60" u 1:($2*q/x3) w l lw 2 lc rgb "blue" t"", 0.0 w l lt -1 t""


set size 0.425,0.4
set origin 0.45,0.55

set ytics 0.1
set xtics 0.05
unset key

unset ylabel
unset xlabel

p[0.9:1.0][:0.25]"poor_asd0.60" u 1:($2*q/x3) w l lw 2 lc rgb "red" t"","good_asd0.60" u 1:($2*q/x3) w l lw 2 lc rgb "blue" t"", 0.0 w l lt -1 t""

q
