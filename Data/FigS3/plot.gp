#q = 1.6e-19; x3 = 0.027e-9;
q = 13.656549; x3 = 1;

#set label 11 "(a)" at 0.02,0.40

set term post color enhanced solid eps ",20"

#set zlabel "{/Symbol r}(x,y) [reduced unit]" #offset 1.6,0.0
set xlabel "x"
set ylabel "y"

set size square

set view map
set pm3d at b map

set cbrange [0:0.8]

#set ytics 0.1
#set key bottom right sample 0.95 spacing 1.65

set title "{/Symbol S} = 0.10 C/m^2 (wo/Polr)"; set output "NoPolr0.10.eps"; sp "xy0.10" u ($1*30):($2*30):($4-$5)  t""
set title "{/Symbol S} = 0.12 C/m^2 (wo/Polr)"; set output "NoPolr0.12.eps"; sp "xy0.12" u ($1*30):($2*30):($4-$5) t""
set title "{/Symbol S} = 0.14 C/m^2 (wo/Polr)"; set output "NoPolr0.14.eps"; sp "xy0.14" u ($1*30):($2*30):($4-$5) t""
set title "{/Symbol S} = 0.16 C/m^2 (wo/Polr)"; set output "NoPolr0.16.eps"; sp "xy0.16" u ($1*30):($2*30):($4-$5) t""




q
