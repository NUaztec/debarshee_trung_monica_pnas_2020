#q = 1.6e-19; x3 = 0.027e-9;
q = 13.656549; x3 = 1;

set term post color enhanced solid eps ",28"
set output "NoPolr.eps"

set multiplot
set size 1,1
set origin 0,0

set label 11 "(a)" at 0.05,0.20


#set label 23 "   Charge \n Amplification" at -0.081,0.6 font ",24"
#set arrow 1 from -0.066,0.48 to -0.066, 0.4 lw 2

set label 23 "{/ZapfDingbats \110}" at -0.073,0.43 tc rgb "red" font ",28"

set ylabel "{/Symbol r}(z) [reduced unit]" font ",34"
set xlabel "z / H" font ",34"

set ytics 0.1

set key bottom right sample 0.95 spacing 1.65

p[-0.05:1.05][-0.25:0.25] "asd0.30" u 1:($2*q/x3) w l lw 2 lc rgb "red" t"f_q = 0.30", "asd0.60" u 1:($2*q/x3) w l lw 2 lc rgb "dark-green" t"f_q = 0.60", "asd0.90" u 1:($2*q/x3) w l lw 2 lc rgb "blue" t"f_q = 0.90", 0.0 w l lt -1 t""

unset label 11

set size 0.45,0.4
set origin 0.23,0.165

set ytics 0.1
set xtics 0.05
unset key

unset ylabel
unset xlabel


p[-0.:0.1][-0.25:]"asd0.30" u 1:($2*q/x3) w l lw 2 lc rgb "red" t"{/Symbol e} = 30","asd0.60" u 1:($2*q/x3) w l lw 2 lc rgb "dark-green" t"{/Symbol e} = 50","asd0.90" u 1:($2*q/x3) w l lw 2 lc rgb "blue" t"{/Symbol e} = 80", 0.0 w l lt -1 t""


set size 0.425,0.4
set origin 0.45,0.565

set ytics 0.1
set xtics 0.05
unset key

unset ylabel
unset xlabel

p[0.9:1.0][:0.25]"asd0.30" u 1:($2*q/x3) w l lw 2 lc rgb "red" t"{/Symbol e} = 30","asd0.60" u 1:($2*q/x3) w l lw 2 lc rgb "dark-green" t"{/Symbol e} = 50","asd0.90" u 1:($2*q/x3) w l lw 2 lc rgb "blue" t"{/Symbol e} = 80", 0.0 w l lt -1 t""

q
