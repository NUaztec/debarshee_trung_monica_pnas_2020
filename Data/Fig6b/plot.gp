set key at 0.112,4.85 sample 0.25 spacing 1.6

set xtics 0.04
set size 0.9,0.9

#set title "Rigid Polymer"
set label "(b)" at 0.18,0.5

set ytics 1.0

set ylabel "U_+ [mJ]"
set xlabel "{/Symbol S}[Cm^{-2}]"

p[0:0.2][0:] "YesPolr.d" eve 2:::0::0 u 1:(1000*$2*$4*$4*0.5) w lp pt 5 ps 3 lc rgb "red" t"{/Symbol e}_2 = 80, w/Polr", "" eve 1:::1::1 u 1:(1000*$2*$4*$4*0.5) w lp pt 7 ps 3 lc rgb "blue" t"{/Symbol e}_2 = 40, w/Polr"

set term post color solid eps enhanced ",32"
set output "Eps40n80_En.eps"
repl
q
