
set key bottom right sample 0.25 spacing 1.8

set xtics 0.04
set size 0.9,0.9



#set title "Rigid Polymer"

set label "(a)" at 0.01,50

set ylabel "{/Symbol F}_+ [mV]"
set xlabel "{/Symbol S} [Cm^{-2}]"

p[:0.20][15:55] "YesPolr.d" eve 2:::0::0 u 1:($4*1000) w lp pt 5 ps 3 lc rgb "red" t"{/Symbol e}_2 = 80, w/Polr", "" eve 1:::1::1 u 1:($4*1000) w lp pt 7 ps 3 lc rgb "blue" t"{/Symbol e}_2 = 40, w/Polr"

set term post color solid eps enhanced ",32"
set output "Eps40n80.eps"
repl
q
