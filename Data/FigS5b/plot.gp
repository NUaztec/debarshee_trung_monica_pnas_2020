#q = 1.6e-19; x3 = 0.027e-9;
q = 13.656549; x3 = 1;

set label 11 "(b)" at 0.1,0.08

set term post color enhanced solid eps ",26"

#set zlabel "{/Symbol r}(x,y) [reduced unit]" #offset 1.6,0.0
set xlabel "{/Symbol r}"
set ylabel "Histogram({/Symbol r})"


set ytics 0.02
set key top right sample 0.5 spacing 1.05

set title "w/Polr"; set output "w_Polr.eps"; 

p[][:0.1] "hist0.10" w lp t"{/Symbol S} = 0.10 C/m^2","hist0.12" w lp t" = 0.12 C/m^2","hist0.14" w lp t" = 0.14 C/m^2","hist0.16" w lp t" = 0.16 C/m^2"



q
