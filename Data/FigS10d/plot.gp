set key top left sample 0.25 spacing 1.8

set xtics 0.2

#set title "Rigid Polymer"
set label "(d)" at 0.8,0.575

set ytics 0.1

set ylabel "U_+ [mJ]"
set xlabel "{/Symbol S}[Cm^{-2}]"

p[0.2:1][0.35:] "wop_Capa.d" eve ::2 u 1:(1000*$2*$4*$4/2) w lp pt 5 ps 3 lc rgb "red" t"wo/Polr", "wp_Capa.d" eve ::2 u 1:(1000*$2*$4*$4/2) w lp pt 7 ps 3 lc rgb "blue" t"w/Polr"

set term post color solid eps enhanced ",32"
set output "GoodSolventEn.eps"
repl
q
