
set ylabel "{/Symbol F}_+ + {/Symbol F}_- [mV]"
set xlabel "{/Symbol S} [Cm^{-2}]"

set key bottom right sampl 0.7


set label "(b)" at 0.05,280

p "wop_Capa.d" u 1:(1000*($4+$5)) w lp pt 5 lc rgb "red" t"wo/Polr", "wp_Capa.d" u 1:(1000*($4+$5)) w lp pt 7 lc rgb "blue "t"w/Polr", 

set term post color eps enhanced ",24" lw 2
set out "TotalCapaFull.eps"
repl 
q
