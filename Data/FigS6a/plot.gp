
set ylabel "{/Symbol F}_+ [mV]"
set xlabel "{/Symbol S} [Cm^{-2}]"

set key bottom right sampl 0.7

set label "(a)" at 0.05,70

p"wop_Capa.d" u 1:($4*1000) w lp pt 5 ps 1 lc rgb "red" t"wo/ Polr","wp_Capa.d" u 1:($4*1000) w lp pt 7 ps 1 lc rgb "blue" t"w/Polr"

set term post color eps enhanced ",24" lw 2
set out "CapaFull.eps"
repl
q
