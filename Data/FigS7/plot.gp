
set key bott right sampl 0.7 spac 1.8

set xtics 0.05

#set title "Electrolyte 3:1"


set ylabel "{/Symbol F}_+ [mv]"
set xlabel "{/Symbol S} [Cm^{-2}]"

p "YesPolr.d" u 1:($4*1000) w lp pt 5 ps 3 lc rgb "red" t"w/Polr", "NoPolr.d" u 1:($4*1000) w lp pt 7 ps 3 lc rgb "blue" t"wo/Polr"

set term post color solid eps enhanced ",32"
set output "electro3.eps"
repl
q
