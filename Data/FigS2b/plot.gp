set term post color eps enhanced ",24" lw 2
set out "DP_wPolr.eps"


set multiplot

set size 1,1
set origin 0,0

set ylabel "{/Symbol F}(z)"
set xlabel "z/H"

set label 11 "(b)" at 0.1, -0.02

p[:0.6][]"asd0.16" u 1:7 w l lw 1 lc rgb "red" t"{/Symbol S} = 0.16","asd0.18" u 1:7 w l dt 2 lc rgb "black" t"{/Symbol S} = 0.18", "asd0.20" u 1:7 w l dt 3 lc rgb "blue" t"{/Symbol S} = 0.20"

unset label 11

set ytics 0.01
set xtics 0.1

set size 0.45,0.45
set origin 0.4,0.15


p[0.4:0.6][-0.055:-0.04]"asd0.16" u 1:7 w l lw 2 lc rgb "red"  t"","asd0.18" u 1:7 w l dt 2 lw 2 lc rgb "black"  t"", "asd0.20" u 1:7 w l dt 3 lw 2 lc rgb "blue" t""

