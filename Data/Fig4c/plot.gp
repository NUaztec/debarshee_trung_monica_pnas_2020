
set ylabel "U_+ [mJ]" offset 1.5,0.0
set xlabel "f_q"

set xtics 0.2
set ytics 0.1

set label 101 "(c)" at 0.24,0.56

#set ytics 0.0025
#set key at 0.5,0.525 sample 1.0 spacing 1. font ",22"
set key vert maxrows 2 at 1,1.1 sample 0.8 width -19 spacing 1.2 font ",28"

p[0.2:1][:1.11] "Capa_1.d" eve ::2 u 1:(1000*0.5*$2*$4*$4) w lp lt 6 lw 2 pt 5 ps 2.5 lc rgb "red" t"Polyelectrolyte:   w/Polr", "Capa_2.d" eve ::2 u 1:(1000*0.5*$2*$4*$4) w lp ps 2.5 lt 4 lw 2 pt 11 lc rgb "brown" t"Electrolyte:  w/Polr", "Capa_3.d" eve ::2 u 1:(1000*0.5*$2*$4*$4) w lp ps 2.85 pt 7 lw 2 lt 2 lc rgb "dark-green" t", wo/Polr", "Capa_4.d" eve ::2 u 1:(1000*0.5*$2*$4*$4) w lp ps 2.85 lt 4 lw 2 pt 9 lc rgb "blue" t", wo/Polr"

set term post color eps enhanced ",34"
set out "energy.eps"
repl
q
