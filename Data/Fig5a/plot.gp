
set key top right sample 0.25 spacing 1.8

set xtics 0.04
set size 0.9,0.9

#set title "Rigid Polymer"

set label "(a)" at 0.01,14

set ylabel "{/Symbol F}_+[mV]"
set xlabel "{/Symbol S}[Cm^{-2}]"

p[][:43] "YesPolr.d" eve 2 u 1:($4*1000) w lp pt 5 ps 3 lc rgb "red" t"w/Polr", "NoPolr.d" eve 2 u 1:($4*1000) w lp pt 7 ps 3 lc rgb "blue" t"wo/Polr"

set term post color solid eps enhanced ",32"
set output "rigidity.eps"
repl
q
