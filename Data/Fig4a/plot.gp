set ylabel "{/Symbol F}(z) [V]" offset 1.5,0.0
set xlabel "z / H"


set key spacing 1.25

set label 101 "(a)" at 0.05,-0.055

set xtics 0.2
#set ytics 0.02

set arrow 111 from 0.6,-0.05495 to 0.999,-0.05495  nohead

p"Polyelectro_asd0.90" u 1:($7+0.001) w l lw 6 lc rgb "dark-green" t"Polyelectrolyte","Electro_asd0.90" u 1:($7+0.001) w l lt 3 lw 5 lc rgb "blue" t"Electrolyte"

set arrow 1 from 0.09,-0.0005 to 0.09,-0.027 lc rgb "blue"  lt -1 lw 3 heads
set arrow 2 from 0.2,-0.0005 to 0.2,-0.04  lc rgb "dark-green"  lt -1 lw 3 heads

set label 11 "{/Symbol F}_+" at 0.10,-0.015

set arrow 11 from 0.85,-0.027 to 0.85,-0.0547 lt -1 lc rgb "blue" lw 2 heads
set arrow 12 from 0.75,-0.039 to 0.75,-0.0547 lt -1 lc rgb "dark-green"  lw 2 heads

set label 12 "{/Symbol F}_{-}" at 0.76,-0.0475

set term post color eps enhanced solid ",34"
set out "Potential.eps"
repl
q
