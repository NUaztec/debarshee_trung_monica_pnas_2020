
q = 13.656549; x3 = 1;

set term post color solid eps enhanced ",34"
set out "peak0mul.eps"; 



set multiplot

set origin 0,0
set size 1,1

set label 101 "(b)" at 0.01, 0.015

set xtics 0.05
#set ytics 0.1;



set ylabel "h_+" font ",38" offset 2.0,0.0
set xlabel "{/Symbol S} [Cm^{-2}]" font ",32"

set key top right samplen 0.6 spaci 1.2 font ",28"

p[:][] "wp_peakAll.d" u 1:($2*q) w lp pt 5 ps 2 lc rgb "red" t"Polyelectrolyte w/Polr","wop_peakAll.d" u 1:($2*q) w lp pt 7 ps 2 lc rgb "blue" t"Polyelectrolyte wo/Polr"

unset multiplot
q
